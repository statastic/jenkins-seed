pipelineJob('data-scraper') {

    description("Scrape and save data")

    definition {
        cpsScm {
            scm {
                git {
                    remote { url('https://gitlab.com/statastic/data-scraper.git') }
                    branches('main')
                    scriptPath('Jenkinsfile')
                    extensions {}
                }
            }
        }
    }
    triggers {
        cron('H 22 * * *')
    }
}